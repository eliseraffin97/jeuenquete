<?php
// src/Controller/HomeController.php 

namespace App\Controller;


// On va utiliser deux classes: response et route
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
class HomeController extends AbstractController
{

    /**
     * @Route("/{qqch}")
     */
    public function home($qqch) {
        
        $number = random_int(0, 100);
        /* return new Response(
            '<html><body>Accueil: 
                vous avez proposé '.$qqch.' et 
                le nombre est '.$number.' 
                </body></html>'
        ); */

        return $this->render(
            'test.html.twig',
            array('qqch' => $qqch, 'number' => $number)
        );

    }
}